/*
 * Simple test for the OLED screen.
 * Wiring:
 *  GND -> GND
 *  VCC -> 3.3V
 *  SCL -> PB6
 *  SDA -> PB7
 */

#include <Wire.h>
#include "SSD1306Ascii.h"
#include "SSD1306AsciiWire.h"

// I2C address of the OLED display
#define I2C_ADDRESS 0x3C

SSD1306AsciiWire oled;
//------------------------------------------------------------------------------
void setup()
{
    Wire.begin();
    Wire.setClock(400000L);

    oled.begin(&Adafruit128x64, I2C_ADDRESS);

    oled.setFont(Adafruit5x7);

    uint32_t m = millis();
    oled.clear();
    oled.println("Hello world!");
    oled.println("A long line may be truncated");
    oled.println();
    oled.set2X();
    oled.println("2X demo");
    oled.set1X();
    oled.print("\nmicros: ");
    oled.print(millis() - m);

    pinMode(PA5, INPUT_ANALOG);

    delay(2000);
    oled.clear();
    
    // Enable scrolling of lines on the display. If this is not enabled, the screen will 
    // not print lines after around 6 displays. To remedy this, use the setCursor() method.
    oled.setScroll(false);
}

void loop() {
    // Read the voltage on pin PA5 and output to the screen
    oled.print(analogRead(PA5) / 4096.0 * 3.3);
    oled.println(" V");
    delay(500);
}


// Loop to use if setScroll(false);
/*
void loop () {
    oled.print(analogRead(PA5) / 4096.0 * 3.3);
    oled.println(" V");
    delay(500);
    // clear() in this case is optional. If not used, the next screen write will overwrite existing characters.
    // However due to the speed of the loop the voltage may not be legible.
    oled.clear();
    oled.home();
}
*/