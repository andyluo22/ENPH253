#include <Arduino.h>
#include <Servo.h>

#include <BridgeLay.h>
#include <Claw.h>
#include <Constants.h>
#include <IR.h>
#include <Motor.h>
#include <PID.h>

#include <Order66.h>

namespace Order66 {
  SlaveState Slave::poll () {

    // print state
    Serial.println("State:");
    Serial.println(static_cast<int>(state));

    // read communication pins
    if(advancePin.checkPinState()) { advanceState(); }

    if(stopPin.checkPinState()) {
      Serial.println("STOP!!!");
      stop();
      return state;
    }

    // reset stopped state
    stopped = false;

    switch(state) {
      case SlaveState::Inactive:
        // keep track of start time
        startFastRampTime = millis();
        stop();
        break;

      case SlaveState::RampClimbing:

        // initial boost
        if (millis() - startFastRampTime < 500) {
          tapeFollow.setMotorSpeed(200);
          tapeFollow.setKP(9);
          tapeFollow.setKD(1);
        } 

        // going up ramp
        else if (!bridgeLayer.sensorOffRamp() && !endRamp) {
          tapeFollow.setMotorSpeed(150);
          tapeFollow.setKP(9);
          tapeFollow.setKD(1);
        }

        // flat part
        else {
          endRamp = true;
          tapeFollow.setMotorSpeed(100);
          tapeFollow.setKP(9);
          tapeFollow.setKD(1);
        }

        tapeFollow.usePID();
        
        // check edgeQRDs
        Serial.print("BL: "); Serial.print(bridgeLayer.getLeftSensor()); Serial.print(" BR: "); Serial.println(bridgeLayer.getRightSensor());
        Serial.println("*******");

        break;

      case SlaveState::HorizontalPreBridge:
        // tapeFollow to edge slowly (maybe ignore tape for a time)
        if(!bridgeLayer.onEdge()) { 
          tapeFollow.setMotorSpeed(70);
          tapeFollow.setKP(8);
          tapeFollow.usePID();
        } else { advanceState(); }

        break;

      case SlaveState::BridgeLaying:
        // align with edge
        bridgeLayer.edgeAlign();
        delay(200);

        // back up (want to turn to be on the right side of tape)
        moveForTime(-70, -80, 400);
        delay(300);

        // lay bridge
        bridgeLayer.layBridge();
        advanceState();
        break;

      case SlaveState::BridgeCrossing:

        // forward cross bridge
        moveForTime(120, 165, 800);

        // turn for tape, look to the right first
        if (!tapeFollow.refindTape(0, 100, 5000)) { 
          // look to left if not found
          tapeFollow.refindTape(100, 0, 10000); 
        }

        // reset tape follow
        tapeFollow.reset();
        tapeFollow.setLostTurningDirection(Side::Left);
        
        advanceState();
        break;

      case SlaveState::HorizontalPreIR:
        // tape follow (maybe higher Kp to make sharper turn)
        tapeFollow.setMotorSpeed(100);
        tapeFollow.setKP(9);
        tapeFollow.usePID();
        break;

      case SlaveState::IRLooking:
        // rotate to best IR angle
        leftMotor.speed(-80);
        rightMotor.speed(80);
        break;

      case SlaveState::IRWaiting:
        // perform tight turn to fit through gate
        if (!irGateTurnComplete) {
          // refind tape
          tapeFollow.refindTape(-110, 110, 5000);
          tapeFollow.reset();
          tapeFollow.setLostTurningDirection(Side::Right);

          irGateTurnComplete = true;
        }

        stop();
        break;

      case SlaveState::HorizontalPostIR:

        // initial speed through arch
        if (millis() - passedGateTimer < 3000) {
          tapeFollow.setMotorSpeed(90);
          tapeFollow.setKP(9);
        }

        // speed between storm troopers
        else {
          tapeFollow.setMotorSpeed(100);
          tapeFollow.setKP(9);
        }

        tapeFollow.usePID();
        break;

      case SlaveState::EdgeSearching:
        // perform turn
        if (!preDetachTurnComplete) { 
          moveForTime(100, -70, 1200);
          preDetachTurnComplete = true;
        }

        // then move forward to edge
        if (!bridgeLayer.onEdge()) { forward(80); }  // slowly go to edge
        else { advanceState(); }

        break;

      case SlaveState::RampDeploying:
        
        // align with edge
        bridgeLayer.edgeAlign();
        delay(200);

        // deploy ramp
        bridgeLayer.deployRamp();
        delay(200);

        // move forward onto stoppers
        moveForTime(50, 50, 2000);
        stop();
        
        advanceState();

        break;

      case SlaveState::Done:
        // complete, so do nothing
        stop();
        break;
    }
    return state;
  }

  bool Slave::advanceState() {
    // ensure doesn't advance state twice
    if(millis() - lastAdvanceTime < SlaveNS::MIN_ADVANCE_TIME) { return false; }
    lastAdvanceTime = millis();
    if (state == SlaveState::Done) { return false; }
    state = static_cast<SlaveState>(static_cast<int>(state) + 1);
    return true;
  }

  void Slave::moveForTime(int leftMotorSpeed, int rightMotorSpeed, int moveTime) {
    leftMotor.speed(leftMotorSpeed);
    rightMotor.speed(rightMotorSpeed);
    delay(moveTime);

    leftMotor.stop();
    rightMotor.stop();
  }
  
  void Slave::stop() {
    if(stopped) { return; }
    stopped = true;
    doneRamp = true;

    // coast
    int coastTime = 100;
    leftMotor.stop();
    rightMotor.stop();
    delay(coastTime);
  }

  void Slave::forward(int speed) {
    leftMotor.speed(speed);
    rightMotor.speed(speed);
  }
}  // namespace Order66