#ifndef diagnostics_h
#define diagnostics_h

#include <Arduino.h>

/**
 *  A simple class to test certain blue pill functionality.
 */
class Diagnostics {
public:
    /**
     * Forces all A and B pins to digital output. 
     * 
     * Because of the USB bootloader PA11 and PA12 will not behave as expected.
     * One needs to comment out the contents of USBSerial.begin() to enable functionality on those pins.
     */ 
    static void outputDigital() {
        for (int i = PA0; i <= PB15; i++) {
            pinMode(i, OUTPUT);
            digitalWrite(i, HIGH);
        }
    }

    /**
     * Outputs PWM-enabled pins to PWM mode.
     * 
     * PA11 is will not output PWM without disabling USB serial.
     */ 
    static void outputPWM() {
        int NUM_PWM_PINS = 16;
        int PWM_PINS[16] = {PA0, PA1, PA2, PA3, PA6, PA7, PA8, PA9, PA10, PA11, PB0, PB1, PB6, PB7, PB8, PB9};
        for (int i = 0; i < NUM_PWM_PINS; i ++) {
            pinMode(PWM_PINS[i], PWM);
            // Just test half duty cycle
            pwmWrite(PWM_PINS[i], 65535/2);
        }
    }

    /**
     * Reads the inputs digitally and prints them to the serial monitor.
     * 
     * Do not test placing HIGH inputs to PA9 and PA10 when testing.
     */ 
    static void readDigital() {
        Serial.begin(9600);
        for (int i = PA0; i <= PB15; i++) {
            pinMode(i, INPUT_PULLDOWN);
        }
        while(true) {
            for (int i = 0; i <= PB15; i++) {
                Serial.print(i);
                Serial.print(":");
                Serial.print(digitalRead(i));
                Serial.print(" ");
            }
            Serial.println();
            delay(1000);
        }
    }

    /**
     * Reads the analog pins and prints them to the serial monitor.
     */ 
    static void readAnalog() {
        Serial.begin(9600);
        int NUM_ANALOG_PINS = 10;
        int ANALOG_PINS[10] = {PA0, PA1, PA2, PA3, PA4, PA5, PA6, PA7, PB0, PB1};
        for (int i = 0; i < NUM_ANALOG_PINS; i++) {
            // Can alternatively use INPUT_ANALOG
            pinMode(ANALOG_PINS[i], INPUT_PULLDOWN);
        }
        while(true) {
            for (int i = 0; i < NUM_ANALOG_PINS; i++) {
                Serial.print(ANALOG_PINS[i]);
                Serial.print(":");
                Serial.print(analogRead(ANALOG_PINS[i]));
                Serial.print(" ");
            }
            Serial.println();
            delay(1000);
        }
    }

    /**
     * Blinks the LED on the Blue Pill. The LED turns ON when the output is LOW.
     */ 
    static void blinkLED() {
        pinMode(LED_BUILTIN, OUTPUT);
        while (true) {
            digitalWrite(LED_BUILTIN, LOW);
            delay(500);
            digitalWrite(LED_BUILTIN, HIGH);
            delay(500);
        }
    }

};
#endif