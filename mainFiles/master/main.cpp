//This is the main.cpp for the master Blue Pill
//To use, replace the main.cpp in src with this.

/**
 * Base Libraries (Wire, Arduino, etc)
 * Imported Libraries (OLED, FreeIMU, etc)
 * Our Libraries
 */
#include <Arduino.h>
#include <Wire.h>

#include <SSD1306Ascii.h>
#include <SSD1306AsciiWire.h>

#include <BridgeLay.h>
#include <Constants.h>
#include <Helper.h>
#include <Motor.h>
#include <Order66.h>
#include <PID.h>

Order66::Master master;
long lastLoopStart = 0l;
void setup() {
  // timer setup
  Helper::timerSetup();
  master.setState(Order66::MasterState::RampClimbing);
}

void loop()
{
  lastLoopStart = millis();
  master.poll();
  // regulate loop time
  Helper::regulateLoop(lastLoopStart, millis());
}
